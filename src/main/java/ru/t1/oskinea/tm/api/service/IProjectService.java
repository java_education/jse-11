package ru.t1.oskinea.tm.api.service;

import ru.t1.oskinea.tm.api.repository.IProjectRepository;
import ru.t1.oskinea.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    public Project updateById(String id, String name, String description);

    public Project updateByIndex(Integer index, String name, String description);

}
